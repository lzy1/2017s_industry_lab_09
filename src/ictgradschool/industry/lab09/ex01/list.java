package ictgradschool.industry.lab09.ex01;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class list {
    /* public static void main() {
         ArrayList list = new ArrayList();
         Character letter = new Character('a');
         list.add(letter);
         if (list.get(0).equals("a")) {
             System.out.println("funny");
         } else {
             System.out.println("Not funny");
         }
     }
     public static void main(String[] args) {
         new list().main();

     }
 */
    /*
    public static void main() {
        ArrayList list = new ArrayList();
        Point pt1 = new Point(3, 4);
        list.add(pt1);
        Point pt2 = (Point) list.get(0);
        pt2.x = 23;
        if (pt2 == pt1) {
            System.out.println("Same object");
        } else {
            System.out.println("Different object");
        }
    }

    public static void main(String[] args) {
        new list().main();
    }
}
/*
    /*

    public static void main() {
        ArrayList list = new ArrayList();
        list.add('a');
        list.add(0, 34);
        String c1 = (String) list.get(1);}


    public static void main(String[] args) {
        new list().main();


*/
    public static void main() {
        String[] array = {"ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN"};

        //code that will convert the array of strings to an ArrayList.
        List<String> list = new ArrayList<>();

        //Using a loop over the index values
        for (int i = 0; i < list.size(); i++) {
            String element = list.get(i).toLowerCase();
            System.out.println(element);

        }
        //Using an enhanced for loop
        for (String element1 : list) {
            System.out.println(element1.toLowerCase());

        }
        //Using an iterator
        Iterator<String> myIterator = list.iterator();
        while (myIterator.hasNext()) {
            String element3 = myIterator.next();
            System.out.println(element3);
            if (element3.equals("TWO")) {
                myIterator.remove();
            }
        }
    }
    private static boolean While(boolean b) {
        return false;
    }

    public static void main(String[] args) {
        new list().main();
    }

}